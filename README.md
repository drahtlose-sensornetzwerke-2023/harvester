The phone portion of the project has three components:
  - InfluxDB (the time series database)

    Time series database means that it saves data as measurements and associated time points. It runs on port 8086 by default and some of the code assumes this.

  - Grafana (the data explaration server)

    It is used as the interface to the database. It allows running queries on the database to inspect the data and modify or delete it.

  - Harvester (the custom code here)

## Set Up
Grafana and InfluxDB expects a GNU/Linux-like environment. This can normally be provided by Termux, which you can install through F-Droid. Unfortunately, Grafana and InfluxDB packages for Termux are either old or non-functional. For this reason:
  - install proot-distro from the Termux repositories (should be something like `pkg install proot-distro`) 
  - set up a openSUSE environment (it is the only distro with the necessary packages at the time of writing) 

    For this step, check `proot-distro --help`. Afterwards, install the packages to the openSUSE (something like `zypper in grafana influxdb`, check openSUSE docs).

For `harvester`:
  - clone the repository
  - run `python3 -m venv .venv` to create a venv (so that there are no conflicts with the system packages)
  - activate it with `source .venv/bin/activate` (assumes the shell is bash, which is the default)
  - run `pip install --editable .` in the project directory
    
    `--editable` installs in editable mode. Editable mode ensures that the edits you make to the script will be active when you restart the code.

    `.` stands for current directory and signals that we want to install the project in the current directory.

Do not directly run the script. `pip` ensures that the dependencies (as declared in the `pyproject.toml` file) are installed. After install, the server can be run by running `harvester` in the command prompt.

You can run all 3 programs by running

```bash
    grafana server & influxdb & harvester
```

> Grafana and InfluxDB will be moved to the background (because of the ampersand).

To stop them from outputting to the stdout, direct their output to /dev/null:

```bash
    grafana server > /dev/null & influxdb & harvester
```

> In this example only Grafana is silenced since it is the 'noisiest' one. The output from influxdb is moderate and mostly useful.

You can make all processes stop by typing Ctrl+C (exits harvester) and Ctrl+D (exits the openSUSE shell). Then reenter.

If you run the commands above multiple times without killing the old ones, you will have multiple servers running (at least for InfluxDB). Just exit them to be sure.

## Protobuf Stuff
If the protobuf structure in the `sensor` code changes, the Python code needs to be [regenerated](https://protobuf.dev/getting-started/pythontutorial/#compiling-protocol-buffers) and the `MESSAGE_SIZE` variable needs to be updated.

## Grafana
  - Connect via HTTP, not HTTPS.
  - Remove `time` clause from the `group_by` line on the default query.
  - Pay attention to time span when inspecting data on Grafana

    If the time span Grafana queries is set incorrectly, no data will be returned.

  - Drop table to inspect the database more easily

    Click the pen icon on the right side of the query editor and use
    ```
        DROP DATABASE "mydb"; CREATE DATABASE "mydb"
    ```
    as the query to remove old data.

## Harvester Development Tips
- Use `import pdb; pdb.set_trace()` when necessary

    At any point of the code that you want to inspect the variables or test code,
    you can use the line above to get a REPL at that point of the code.

- Use curl during development
   
   If you want to check the protobuf parsing, you can save the binary with the protobuf data from the SD card and send it via curl with
   ```bash
   curl -i -XPOST "http://<harvester-ip>:8000/" --data-binary @<file-name>
   ```
   If you connect to the phone's access point, harvester-ip is the gateway address. If you run the server on the computer itself, you can use localhost in place of the IP address.

## General Tips
- Use `guiscrcpy`

    It is combursome to code on the phone using the touch screen. At least on
    a Linux desktop, one can use a program called `guiscrcpy` to mirror the
    phone screen to a computer and type on the phone via the computer's keyboard.
