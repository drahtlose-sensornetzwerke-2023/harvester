#! /usr/bin/env python3
import influxdb_client
import logging
import reactivex as rx
from http.server import HTTPServer, SimpleHTTPRequestHandler
from influxdb_client import InfluxDBClient, Point, WritePrecision
from reactivex import operators as ops
from .measurement_pb2 import Measurement

def run(handler_class, server_class=HTTPServer):
    server_address = ('', 8000)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

def pointize(m):
    try:
        return Point("sensors") \
        .field("humidity", m.humidity) \
        .field("temperature", m.temperature) \
        .field("raw_voc", m.raw_voc) \
        .field("raw_nox", m.raw_nox) \
        .time(m.unixtime, write_precision=WritePrecision.S)
    except Exception as ex:
        print(type(ex), ex)

class HarvesterHandler(SimpleHTTPRequestHandler):
    def do_POST(self):
	# We have to send response so that the client does not hang waiting for a response
        self.send_response(200)
        self.send_header('Content-Length', '0')
        self.end_headers()

        post_body = self.rfile.read()

        def delimiter(stream):
            i = 0
            while i < len(stream):
                msg_len = stream[i]
                # Move index to the beginning of the message, simplifies the next 2 lines
                i += 1
                yield stream[i:(i+msg_len)]
                i += msg_len
            return

        # ReactiveX is pulled by InfluxDB already, we can as well use it.
        obs = rx.from_iterable(delimiter(post_body)).pipe(
            ops.map(Measurement().FromString),
            ops.map(pointize),
            ops.catch(print)
        )
        # Be careful with ReactiveX. Any stage of the pipe can fail silently.
        # You can use the following line to see if the expected things are
        # coming out of the pipeline.
        # obs.subscribe(print)

        with InfluxDBClient(url="http://localhost:8086", org="my-org") as client:
        # For our use case (ingesting from ReactiveX Observable), the write_option
        # parameter must not be "SYNCHRONOUS"

        # The reason we are not simply iterating over the Points and write them
        # is that that would be inefficient. Bulk transfers are faster.
            write_api = client.write_api(debug=True)
            write_api.write(bucket="mydb", record=obs)

def main():
    run(HarvesterHandler)
